/*****************************************************************************
 * decoder_mad.c
 * this file is part of https://github.com/ouistiti-project/putv
 *****************************************************************************
 * Copyright (C) 2016-2017
 *
 * Authors: Marc Chalain <marc.chalain@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>

#include "player.h"
#include "decoder.h"
#include "filter.h"

#define err(format, ...) fprintf(stderr, "\x1B[31m"format"\x1B[0m\n",  ##__VA_ARGS__)
#define warn(format, ...) fprintf(stderr, "\x1B[35m"format"\x1B[0m\n",  ##__VA_ARGS__)
#ifdef DEBUG
#define dbg(format, ...) fprintf(stderr, "\x1B[32m"format"\x1B[0m\n",  ##__VA_ARGS__)
#else
#define dbg(...)
#endif

#define decoder_dbg(...)

decoder_t *decoder_build(player_ctx_t *player, const char *mime, const filter_t *filter)
{
	decoder_t *decoder = NULL;
	const decoder_ops_t *ops = NULL;
	decoder_ctx_t *ctx = NULL;
#ifdef DECODER_MAD
	if (mime && !strcmp(mime, decoder_mad->mime))
	{
		ops = decoder_mad;
	}
#endif
#ifdef DECODER_FLAC
	if (mime && !strcmp(mime, decoder_flac->mime))
	{
		ops = decoder_flac;
	}
#endif
#ifdef DECODER_PASSTHROUGH
	if (mime && !strcmp(mime, decoder_passthrough->mime))
	{
		ops = decoder_passthrough;
	}
#endif

	if (ops != NULL)
	{
		ctx = ops->init(player, filter);
	}
	if (ctx != NULL)
	{
		decoder = calloc(1, sizeof(*decoder));
		decoder->ops = ops;
		decoder->ctx = ctx;
	}
	return decoder;
}
